// create a standard server app using Node.JS.
// get the http module using the require() directive. Repackage the module on a new variable
const http = require('http');
const host = 4000;
// create the server and place it inside a new variable to give it an identifier
let server = http.createServer((request, response) => {
	response.end('Welcome to the app!');
});
// Assign a designated port that will serve the project, by binding the connection with the desired port.
server.listen(host);
console.log(`Listening on port: ${host}`);